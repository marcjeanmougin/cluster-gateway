Cluster gateway
===============

A small django app designed to allow access to a computing cluster for user authenticated through a federated login (SAML2/Shibboleth). You need to expose the web port (for the gateway) and an UDP port (for Wireguard).


HOWTO
-----

- Install `apt install wireguard apache2 libapache2-mod-shib libapache2-mod-wsgi-py3 python3-certbot-apache python3-django python3-ldap3` and configure them well. We'll assume you have a shibboleth auth page setup on `/auth` (with your WAYF). You might need `WSGIApplicationGroup %{GLOBAL}` for obscure reasons. 
- Install https://github.com/Brown-University-Library/django-shibboleth-remoteuser
- Allow www-data to run wg (add `www-data ALL=(ALL) NOPASSWD:/usr/bin/wg` to `/etc/sudoers.d/10-www-data`)
- Install and configure a LDAP server that will be used to provide accounts within the cluster ([doc for ubuntu](https://ubuntu.com/server/docs/service-ldap))
- Generate a django secret key (`from django.core.management.utils import get_random_secret_key ; get_random_secret_key()`)
- Prepare a configuration folder for wireguard. It should be write-accessible by www-data, and contain a file with your private key wg0.conf.base.stripped with
```
[Interface]
ListenPort = 51820
PrivateKey = <your private key>
```

You can also prepare your initial /etc/wireguard/wg0.conf file with
```
[Interface]
Address = 10.10.0.1/16
ListenPort = 51820
PrivateKey = <your private key>
DNS = 8.8.8.8

PostUp = iptables -A FORWARD -o %i -j ACCEPT; iptables -A FORWARD -i %i -j ACCEPT; iptables -t nat -A POSTROUTING -o ens3 -j MASQUERADE
PostDown = iptables -D FORWARD -o %i -j ACCEPT;iptables -D FORWARD -i %i -j ACCEPT; iptables -t nat -D POSTROUTING -o ens3 -j MASQUERADE
```
(replace your DNS, interface (`ens3` here), and wireguard port, if needed)

- Prepare a configuration folder for ssh. It should be readable by www-data, and contain a secure ssh keypair (see `man ssh-keygen`). This key should never leak. You will enroll its public key into your machines with `TrustedUserCAKeys` parameter in `/etc/ssh/sshd_config`
- `mv localsettings.py.example localsettings.py`
- TODO I probably still have stuff hardcoded and not put in localsettings.py . Sorry about it 
- `wg-quick up wg0` to start wireguard. Make sure you have `net.ipv4.ip_forward=1`
- If needed, add iptable rules to prevent unwanted access to some of your network to wireguard clients
