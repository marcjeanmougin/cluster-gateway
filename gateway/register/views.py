from django.shortcuts import render, redirect
from django.contrib.auth.models import User, AnonymousUser
from django.template.loader import render_to_string
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseForbidden, HttpResponseRedirect
from .models import WireguardKey, SSHKey, Profile

import base64
import datetime
import itertools
from ldap3 import Server, Connection, ALL
import os
import re
import sys
import tempfile

from localsettings import SUFFIXMAP, LDAPDC, LDAPPASSWORD, SIGNINGKEYFILE, WGCONFFILE


def username_from_uid(eppn):
    suffixes = SUFFIXMAP
    for i in suffixes.keys():
        m = re.search(r"(.*)@"+i, eppn)
        if m:
            return suffixes[i]+'-'+m[1]

# Create your views here.
def index(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect("/auth")
    wgk = WireguardKey.objects.filter(user=request.user)
    sshk = SSHKey.objects.filter(user=request.user)
    account = username_from_uid(request.user.username)
    if not hasattr(request.user, 'profile'):
        request.user.profile = Profile.objects.create(user = request.user, is_admin=False, is_trusted = False)
        request.user.profile.save()
    #request.user.profile.is_admin = True
    #request.user.profile.save()
    conn = Connection('localhost', 'cn=admin,'+LDAPDC, LDAPPASSWORD, auto_bind=True)
    conn.search(LDAPDC, '(objectClass=posixAccount)')
    numaccounts = len(conn.entries)

    if not conn.search(LDAPDC, '(uid='+account+')'):
        conn.add('uid='+account+',ou=People,'+LDAPDC, ['account', 'posixAccount'], {"userPassword": "{CRYPT}x", "loginShell": "/bin/bash", "homeDirectory": "/home/"+account, "gidNumber": "5000", "cn": account, "uidNumber": str(10000 + numaccounts)})
    ans = render_to_string('index.html', request=request, context={'wgk': wgk, 'sshk': sshk, 'user': request.user, 'account' : account, 'us': User.objects.all(), 'tomorrow': datetime.date.today() + datetime.timedelta(days=1), 'account': account })

    #print(ans, file=sys.stderr)
    return HttpResponse(ans)

def addssh(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect("/auth")
    ssh = SSHKey.objects.create(
            pubkey = request.POST["key"],
            user = request.user,
            expdate = datetime.datetime.today(), 
            signedpubkey = "",
            )
    ssh.save()
    return redirect('index')

def extendsssh(request):
    if not request.user.profile.is_trusted:
        return HttpResponseForbidden()
    key = request.POST["key"]
    account = username_from_uid(request.user.username)
    fin = tempfile.NamedTemporaryFile(delete=False)
    fin.file.write(key.encode())
    fin.close()
    os.system("ssh-keygen -s "+SIGNINGKEYFILE+" -V +52w -I "+account+" -n "+account+" " + fin.name)
    try:
        fout = open(fin.name + "-cert.pub", 'r')
        signed = fout.read()
        fout.close()
        os.remove(fin.name + "-cert.pub")
        SSHKey.objects.filter(user=request.user).filter(pubkey=key).update(signedpubkey=signed, expdate = datetime.datetime.today() + datetime.timedelta(weeks = 52))
    except:
        return HttpResponseBadRequest("Cound not sign your key, please double check SSH key format.")
    return redirect('index')

def enablewg(request): #TODO
    if not request.user.profile.is_trusted:
        return HttpResponseForbidden()
    key = request.POST["key"]
    k = WireguardKey.objects.filter(user=request.user).get(pubkey=key)
    k.active = not k.active
    k.save()
    updatewg()
    return redirect('index')

def trust(request):
    if not request.user.profile.is_admin:
        return HttpResponseForbidden()
    u = request.POST["user"]
    uu = User.objects.get(username=u)
    uu.profile.is_trusted = not uu.profile.is_trusted
    uu.profile.save()
    return redirect('index')

def makeadmin(request):
    if not request.user.profile.is_admin:
        return HttpResponseForbidden()
    u = request.POST["user"]
    uu = User.objects.get(username=u)
    uu.profile.is_admin = True
    uu.profile.save()
    return redirect('index')

def updatewg():
    f = open(WGCONFFILE+".base.stripped", 'r')
    a = f.read()
    f.close()
    for wg in WireguardKey.objects.filter(active=True):
        a+="[Peer] #" + wg.user.username + "\nPublicKey = "+wg.pubkey+"\nAllowedIPs = "+wg.ip+"/32\nPersistentKeepalive = 25\n\n"
    f = open(WGCONFFILE, 'w')
    f.write(a)
    f.close()
    os.system("sudo wg syncconf wg0 "+WGCONFFILE)
    return

def addwg(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect("/auth")
    #not efficient but we don't care
    key = request.POST["key"]
    try:
        if len(base64.b64decode(key)) != 32:
            return HttpResponseBadRequest("Bad key format")
    except:
        return HttpResponseBadRequest("Bad key format")
    if len(key) != 44 or key[43] != '=':
        return HttpResponseBadRequest("Bad key format")
    for (i,j) in itertools.product(range(254), range(254)):
        if not WireguardKey.objects.filter(ip="10.10."+str(i+1)+"."+str(j+1) ):
            ip = "10.10."+str(i+1)+"."+str(j+1)
            break
    wg = WireguardKey.objects.create(
            pubkey = key,
            user = request.user,
            ip = ip,
            )
    wg.save()
    return redirect('index')

def delssh(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect("/auth")
    key = request.POST["key"]
    SSHKey.objects.filter(user=request.user).filter(pubkey=key).delete()
    return redirect('index')

def delwg(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect("/auth")
    key = request.POST["key"]
    WireguardKey.objects.filter(user=request.user).filter(pubkey=key).delete()
    return redirect('index')
