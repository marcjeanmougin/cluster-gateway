from django.db import models

# Create your models here.
from django.conf import settings
from django.dispatch import receiver
from django.db.models.signals import post_save
from localsettings import WGPUB

User=settings.AUTH_USER_MODEL

class WireguardKey(models.Model):
    user = models.ForeignKey(User, default=1, null=True, on_delete=models.SET_NULL,)
    pubkey = models.CharField(max_length=100)
    ip = models.GenericIPAddressField()
    active = models.BooleanField(default = False)

    def __str__(self):
        return """[Interface]
Address = {ip}/32
PrivateKey = <Your private key>
#Corresponding pubkey we know is {pubkey}

[Peer]
PublicKey = {wgp}
PersistentKeepAlive = 25
Endpoint = mesogip.r2.enst.fr:51820
AllowedIPs = 0.0.0.0/0
        """.format(pubkey = self.pubkey, ip = self.ip, wgp = WGPUB)

class SSHKey(models.Model):
    user = models.ForeignKey(User, default=1, null=True, on_delete=models.SET_NULL,)
    pubkey = models.TextField(max_length=2000)
    signedpubkey = models.TextField(max_length=2000)
    expdate = models.DateField()

    def __str__(self):
        return """{pubkey}
signed SSH cert until {expdate}: 
{signedpubkey}""".format(pubkey = self.pubkey, expdate = self.expdate, signedpubkey = self.signedpubkey)

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    is_admin = models.BooleanField(default = False)
    is_trusted = models.BooleanField(default = False)


#this method to generate profile when user is created
@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

#this method to update profile when user is updated
@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
