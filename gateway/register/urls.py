from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("addwg", views.addwg, name="addwg"),
    path("addssh", views.addssh, name="addssh"),
    path("delwg", views.delwg, name="delwg"),
    path("delssh", views.delssh, name="delssh"),
    path("extendsssh", views.extendsssh, name="extendsssh"),
    path("trust", views.trust, name="trust"),
    path("makeadmin", views.makeadmin, name="makeadmin"),
    path("enablewg", views.enablewg, name="enablewg"),
]
